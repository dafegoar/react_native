import React, { useState } from "react";
import { SocialIcon } from "react-native-elements";
import * as firebase from "firebase";
import * as Facebook from "expo-facebook";
import { FacebookApi } from "../../utils/Social";
import Loading from "../Loading";

export default function LoginFacebook(props) {
  const { toastRef, navigation } = props;
  const { isLoading, setIsLoading } = useState(false);

  const login = async () => {
    const {
      type,
      token
    } = await Facebook.logInWithReadPermissionsAsync(
      FacebookApi.application_id,
      { permissions: FacebookApi.permissions }
    );
    if (type === "success") {
      setIsLoading(true);
      const credentials = firebase.auth.FacebookAuthProvider.credential(token);
      await firebase
        .auth()
        .signInWithCredential(credentials)
        .then(() => {
          navigation.navigate("MyAccount");
        })
        .catch(() => {
          toastRef.current.show(
            "Error accediendo con Facebook, intentelo mas tarde"
          );
        });
    } else if (type === "cancel") {
      toastRef.current.show("Inicio de sesion Cancelado");
    } else {
      toastRef.current.show("Error desconocido, Intentelo más tarde");
    }
    setIsLoading(false);
  };

  return (
    <>
      <SocialIcon
        title="iniciar sesión con Facebook"
        button
        type="facebook"
        onPress={isLoading}
      />
      <Loading isVisible={false} text="Iniciando Sesión" />
    </>
  );
}
