import firebase from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyCOJjKar6OKti_jDLsnhs_nZwz2UP9i6KM",
  authDomain: "tenedores-79363.firebaseapp.com",
  databaseURL: "https://tenedores-79363.firebaseio.com",
  projectId: "tenedores-79363",
  storageBucket: "tenedores-79363.appspot.com",
  messagingSenderId: "1021850205597",
  appId: "1:1021850205597:web:82b5322b715f807740c374"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
